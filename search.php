<?

#################################################################
require ("libs/fo_prepare.php");

$what = "general";

$z	=	explode("/", $data->GET["url"]);
for ($i=0; $i<count($z); $i++) {
	$k	=	explode(".-", $z[$i]);
	$data->GET[$k[0]]	=	$k[1];
}
$data->GET["find"]	=	$z[(count($z)-1)];

$t->set_file(array(
    "index"		=>	"search.tpl.htm"
));

if (strlen($data->POST["what"])) $what = $data->POST["what"];
if (strlen($data->GET["what"])) $what = $data->GET["what"];

$actions = array("general");
if (!in_array($what, $actions)) $what = "general";
$blocks->HideBlocks($what, $actions, "index");

if ($what == "general") {
	$MESSAGES["site_title"]		=	htmlspecialchars($data->GET["find"]);

	$t->set_var(array(
		"SCRIPT_FIND"		=>	$data->GET["find"],
		"SCRIPT_PRICE_MIN"	=>	$data->GET["min"],
		"SCRIPT_PRICE_MAX"	=>	$data->GET["max"],
		"SCRIPT_SIZE"		=>	$data->GET["size"],
		"SCRIPT_COLOR"		=>	$data->GET["color"],
		"SCRIPT_VENDOR"		=>	$data->GET["vendor"],
		"SCRIPT_URL"		=>	"/s"
	));

	$t->set_block("index", "found_products", "__found_products");
	$t->set_block("found_products", "av_sizes", "_av_sizes");

	$found	=	$cp->SearchInProducts($data->GET["find"]);

	$found_all	=	$found;

	$found	=	$cp->ProductsDoFilters($found_all, array($data->GET["min"], $data->GET["max"]), $data->GET["size"], $data->GET["color"], $data->GET["vendor"]);

	if (count($found)) {
		$blocks->HideBlock("index", "notfound_products");
	} else {
		$t->set_var("_found_products", "");
	}

	for ($i=0; $i<count($found); $i++) {
		$product					=	$cp->GetProductByID($found[$i]["id"]);

		if (!$product[rating_count]) $product[rating_count]	=	0.0001;
		$product_rating	=	round(($product[rating_sum] / $product[rating_count]) * 20);
		$sizes	=	$cp->GetProductWarehouseSizes_2($product["id"], $data->GET["color"]);

		if (!count($sizes)) {
			$hide_buy_button	=	"display: none;";
			$t->set_var("_av_sizes", "нет в наличии");
		} else {
			$hide_buy_button	=	"";
			for ($is=0; $is<count($sizes); $is++) {
				if ($is < (count($sizes)-1)) $av_sizes_point = " - "; else $av_sizes_point = "";
				$t->set_var(array(
					"AV_SIZE_NAME"		=>	$sizes[$is]["name"],
					"AV_SIZE_POINT"		=>	$av_sizes_point
				));
   				$t->parse("_av_sizes", "av_sizes", true);
		 	}
		}

		$colors		=	$cp->GetProductColorList($found[$i]["id"]);
		$color		=	$colors[0];

		$t->set_var(array(
			"ITEM_NUM"						=>	($i+1),
			"PRODUCT_ID"					=>	$product[id],
			"PRODUCT_URL"					=>	$product[url],
			"PRODUCT_NAME"					=>	$product[name],
			"PRODUCT_CODE"					=>	$product[code],
			"PRODUCT_VENDOR_NAME"			=>	$product[vendor_name],
			"PRODUCT_VENDOR_COUNTRY"		=>	$product[vendor_name_country],
			"PRODUCT_PRICE"					=>	number_format($product[price], 0, ',', ' '),
			"PRODUCT_FORUNIT"				=>	$product[for_unit],
			"PRODUCT_PRICE2"				=>	number_format($product[price2], 0, ',', ' '),
			"PRODUCT_FORUNIT2"				=>	$product[for_unit2],
			"PRODUCT_WAREHOUSE"				=>	$product[warehouse],
			"PRODUCT_UNIT"					=>	$cp->GetUnitByID($product[units]),
			"PRODUCT_SHORT_DESCRIPTION"		=>	$_descr,
			"PRODUCT_FULL_DESCRIPTION"		=>	$_full_descr,
			"PRODUCT_FULL_DESCRIPTION_PART1"=>	$_full_descr_parts[0],
			"PRODUCT_FULL_DESCRIPTION_PART2"=>	$_full_descr_parts[1],
			"PRODUCT_RATING"				=>	$product_rating,
			"PRODUCT_COLOR"					=>	$color,
			"PRODUCT_SIZES"					=>	count($sizes),
			"PRODUCT_BUY_HIDDEN"			=>	$hide_buy_button
		));
 		$t->parse("__found_products", "found_products", true);
 		$t->set_var("_av_sizes", "");
 		unset($product);
	}

	#############################################################################	фильтр по цене
	$filter_price	=	$cp->GetProductsPriceRange($found_all);
	if ($data->GET["min"]) $_min	=	$data->GET["min"]; else $_min	=	$filter_price["min"];
	if ($data->GET["max"]) $_max	=	$data->GET["max"]; else $_max	=	$filter_price["max"];
	$t->set_var(array(
		"FILTER_PRICE_MIN"			=>	number_format($_min, 0, ',', ' '),
		"FILTER_PRICE_MAX"			=>	number_format($_max, 0, ',', ' '),
		"FILTER_PRICE_MIN_VALUE"	=>	$filter_price["min"],
		"FILTER_PRICE_MAX_VALUE"	=>	$filter_price["max"],
		"FILTER_PRICE_MIN_VALUE0"	=>	$_min,
		"FILTER_PRICE_MAX_VALUE0"	=>	$_max
	));


	$blocks->ShowHeaderBasketInfo();		//	отображаем информацию о корзине в шапке сайта
	$blocks->MainCatsDrop();				//	Выпадающее окно со списком главных разделов
	if (count($found_all)) {
//		$blocks->ShowFiltersRow();				//	показываем фильтры
        $t->set_var("BLOCK_FILTERS_ROW", "");
	} else {
		$t->set_var("BLOCK_FILTERS_ROW", "");
	}


	$t->set_block("index", "index_general", "_index_general");
	$t->parse("_index_general", "index_general", true);
}

$t->parse("OVERALL_HEADER", "overall_header");
$t->parse("OVERALL_FOOTER", "overall_footer");
$t->parse("OUT", "index");

/*
$ff	=	fopen("./logs/auto_".date("Ymd-Hms")."-".microtime(true).".txt", "w");
fputs($ff, $t->finish($t->get_var("OUT"))."\n");
fclose($ff);
*/

$t->p("OUT");
?>